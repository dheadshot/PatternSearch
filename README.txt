PatternSearch
================

Based On https://github.com/dheadshot/ListToCSV

(Compile with kernel32)

A (Globing) Pattern Search commandline tool for Windows, designed as a replacement for Unix's `find $1 -name $2` tool.

Usage: psearch <directory name> <pattern> [f | d]
  <pattern>  Specify the pattern to only list files/directories that correspond, e.g. "*.pdf" to just list PDF files.
  f          Specify "f" to just match files not directories.  Leave this out to match both.
  d          Specify "d" to just match directories not files.  Leave this out to match both.

Copyright (c) 2021 DHeadshot's Software Creations
MIT Licence

