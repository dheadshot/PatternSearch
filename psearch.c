//Based On https://github.com/dheadshot/ListToCSV
//Compile with kernel32
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

DWORD ListFiles(char *szDir, char *fargptr, char *fext, int recurs, char ftype);

int streq_(char *a, char *b)
{
  unsigned long la, lb;
  la = strlen(a);
  lb = strlen(b);
  if (la != lb) return 0;
  if (memcmp(a,b,la)==0) return 1; else return 0;
}

int main(int argc, char *argv[])
{
  HANDLE hFind = INVALID_HANDLE_VALUE;
  char szDir[MAX_PATH];
  char *fargptr, *fpat = NULL, *ftyp=NULL;
  int recurs = 0, fen = 2;
  DWORD dwError=0;
  
  if ((argc < 2 || argc > 4) || ((argc > 3) && (streq_(argv[3],"f")==0) && (streq_(argv[3],"F")==0) && (streq_(argv[3],"d")==0) && (streq_(argv[3],"D")==0)) || (streq_(argv[1],"/?")))
  {
      printf("\nDHSC PatternSearch 0.01.02\nCopyright (c) 2021 DHeadshot's Software Creations\nMIT Licence\nTwitter @DHeadshot\nMastodon @dheadshot@mastodon.social\n");
      printf("Searches for files/directories recursively according to a pattern\n");
          printf("Usage: %s <directory name> <pattern> [f | d]\n", argv[0]);
          printf("\t<pattern>\tSpecify the pattern to only list files/directories that\n\t\tcorrespond, e.g. \"*.pdf\" to just list PDF files.\n");
          printf("\tf\tSpecify \"f\" to just match files not directories.  Leave this\n\t\tout to match both.\n");
          printf("\td\tSpecify \"d\" to just match directories not files.  Leave this\n\t\tout to match both.\n");
          return (1);
  }
 
  fargptr = argv[1];
  fpat = argv[2];
  if (argc>3) ftyp = argv[3];
  else ftyp = "\0\0";
  if (strlen(fargptr) > MAX_PATH - 3 )
  {
    fprintf(stderr,"\nDirectory path is too long.\n");
    return (2);
  }
  strcpy(szDir, fargptr);
  strcat(szDir, "\\*");
#ifdef DEBUG
    fprintf(stderr,"DEBUG: '%s', '%s', '%s', '%d', '%c'\n",szDir, fargptr, fpat, 1, ftyp[0]);
#endif
  dwError = ListFiles(szDir, fargptr, fpat, 1, ftyp[0]);
  return dwError;
}

DWORD ListFiles(char *szDir, char *fargptr, char *fpat, int recurs, char ftype)
{
  HANDLE hFindF = INVALID_HANDLE_VALUE, hFindD = INVALID_HANDLE_VALUE;
  DWORD dwError=0;
  WIN32_FIND_DATA ffdD, ffdF;
  char chDir[MAX_PATH *2], nchDir[MAX_PATH *2], szD2[MAX_PATH*2];

  hFindD = FindFirstFile(szDir, &ffdD);
  if (fpat)
  {
    strcpy(szD2,fargptr);
    strcat(szD2,"\\");
    strcat(szD2,fpat);
    hFindF = FindFirstFile(szD2, &ffdF);
  }
  
  if (INVALID_HANDLE_VALUE == hFindD || fpat && INVALID_HANDLE_VALUE == hFindF) 
  {
    dwError = GetLastError();
    if (INVALID_HANDLE_VALUE != hFindD && dwError == ERROR_FILE_NOT_FOUND)
    {
        //Just because it can't find patterned files, doesn't mean there are no directories to recurse into!
        //This is a dirty bodge but sortof works.
        goto CONTINUEFROMERROR;
    }
    if (INVALID_HANDLE_VALUE != hFindD) FindClose(hFindD);
    if (INVALID_HANDLE_VALUE != hFindF) FindClose(hFindF);
    if (dwError == ERROR_FILE_NOT_FOUND) return ERROR_NO_MORE_FILES;
    fprintf(stderr,"Error Finding File(s).\n");
    return dwError;
  }
CONTINUEFROMERROR:
  fprintf(stderr,"");
  long ic = 0;
  int changed = 0;
  char afn[MAX_PATH] = "\0";
  if (INVALID_HANDLE_VALUE != hFindF)
  {
    if (fpat) do
    {
        if (ftype == 0 || (toupper(ftype)=='F' &&
                           !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_DEVICE) && !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) && !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_VIRTUAL))
                       ||  (toupper(ftype)=='D' && (ffdF.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)))
          printf("%s\\%s\n",fargptr,ffdF.cFileName);
    } while (FindNextFile(hFindF, &ffdF) != 0);
    dwError = GetLastError();
    if (dwError != ERROR_NO_MORE_FILES) 
    { 
      fprintf(stderr, "Error finding file(s)!\n");
      FindClose(hFindD);
      FindClose(hFindF);
      return dwError;
    }
  }
  do
  {
        if (!fpat)
        {
          if (ftype == 0 || (toupper(ftype)=='F'  &&
                             !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_DEVICE) && !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) && !(ffdF.dwFileAttributes & FILE_ATTRIBUTE_VIRTUAL))
                         ||  (toupper(ftype)=='D' && (ffdD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)))
            printf("%s\\%s\n",fargptr,ffdD.cFileName);
        }
        
        
        if (recurs && (ffdD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (!streq_(ffdD.cFileName,".") && !streq_(ffdD.cFileName,"..")))
        {
          strcpy(chDir,fargptr);
          strcat(chDir,"\\");
          strcat(chDir, ffdD.cFileName);
          strcpy(nchDir,chDir);
          strcat(nchDir,"\\*");
          dwError = ListFiles(nchDir,chDir, fpat, recurs, ftype);
          if (dwError != ERROR_NO_MORE_FILES) 
          {
            fprintf(stderr, "Error recursing in %s!\n",chDir);
          }
        }
  } while (FindNextFile(hFindD, &ffdD) != 0);
  dwError = GetLastError();
  if (dwError != ERROR_NO_MORE_FILES) 
  {
     fprintf(stderr, "Error finding file(s)!\n");
  }
  FindClose(hFindD);
  FindClose(hFindF);
  return dwError;
}
